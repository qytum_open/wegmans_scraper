import time, re, json, requests, os
from selenium import webdriver
from conf import Conf
from mailjet_rest import Client

conf = Conf()
mailjet = Client(auth=(conf.get_mail_key(), conf.get_mail_secret()), version='v3.1')

def main():
    print("Starting Wegmans Checker")
    
    profile = webdriver.FirefoxProfile()
    

    profile.set_preference("general.useragent.override", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0")
    profile.set_preference("navigator.webdriver", "False")    
    driver = webdriver.Firefox(executable_path = 'drivers/geckodriver')


    driver.get("https://shop.wegmans.com/")
    time.sleep(3)
    
    #Login
    # Close modal if up
    try:
        modal_button = driver.find_elements_by_xpath("//button[@id='shopping-selector-parent-process-modal-close-click']")[0]
        modal_button.click()
        
        time.sleep(3)
    except Exception:
        pass
    
    try:
        login_button = driver.find_elements_by_xpath("//button[@id='nav-register']")[0]
        login_button.click()
        time.sleep(10)
        
        user_field = driver.find_elements_by_xpath("//input[@id='signInName']")[0]
        user_field.clear()
        user_field.send_keys(conf.get_user_id())
        
        passwd_field = driver.find_elements_by_xpath("//input[@id='password']")[0]
        passwd_field.clear()
        passwd_field.send_keys(conf.get_user_passwd())
        
        remember_field = driver.find_elements_by_xpath("//input[@id='rememberMe']")[0]
        remember_field.click()
        
        time.sleep(1)
        signin_button = driver.find_elements_by_xpath("//button[@id='next']")[0]
        signin_button.click()
        
        time.sleep(20)
        
    except Exception as ex:
        print(ex)
        pass
    
    
    #Goto Cart
    cart_link = driver.find_elements_by_xpath("//a[@id='nav-cart-main-checkout-cart']")[0]
    cart_link.click()
    time.sleep(20)
    
    #Goto Checkout
    checkout_button = driver.find_elements_by_xpath("//button[@id='checkout-cart-top-continue']")[0]
    checkout_button.click()
    time.sleep(20)
    
    
    while True:
        
        #Check for 'No Timeslot Available' every 10min
        try:
            schedule_form = driver.find_elements_by_xpath("//schedule-form")[0]
            schedule_form_txt = schedule_form.text
            
            if not "No Timeslot Available" in schedule_form_txt:
                #Alert there are times
                print("ALERT: FOUND TIMES")
                os.system('spd-say -w -i +100 "ALERT, ALERT, ALERT, WEGMANS TIME HAS BEEN FOUND!"')
                send_simple_message()
                driver.save_screenshot("screenshot.png")
                break
            else:
                print("No times found, sleeping for 10min...")
            
        except Exception as ex:
            print("Something Broke: ",ex)
            break
        
        time.sleep(600)
        
        driver.refresh()
        time.sleep(15)

# -- send email -- #
def send_simple_message():
    data = {
          'Messages': [
            {
              "From": {
                "Email": "",
                "Name": ""
              },
              "To": [
                {
                  "Email": "",
                  "Name": ""
                }
              ],
              "Subject": "ALERT",
              "TextPart": "Times Available -- RUN",
              "HTMLPart": "Times Available -- RUN",
              "CustomID": "AppGettingStartedTest"
            }
          ]
        }
    
    result = mailjet.send.create(data=data)
    print(result.status_code)
    print(result.json())


if __name__ == "__main__":
    main()
